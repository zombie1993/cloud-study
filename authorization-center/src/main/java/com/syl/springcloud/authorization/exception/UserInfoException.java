package com.syl.springcloud.authorization.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @author syl
 * @create 2018-08-06 15:23
 **/
public class UserInfoException extends AuthenticationException {

    public UserInfoException(String message) {
        super(message);
    }

}
