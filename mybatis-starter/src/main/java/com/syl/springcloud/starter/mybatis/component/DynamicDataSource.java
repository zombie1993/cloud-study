package com.syl.springcloud.starter.mybatis.component;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 动态数据源切换
 * @author syl
 * @create 2018-08-29 16:39
 **/
public class DynamicDataSource extends AbstractRoutingDataSource{

     protected Object determineCurrentLookupKey() {
         return DatabaseContextHolder.getDatabaseType();
     }
}
