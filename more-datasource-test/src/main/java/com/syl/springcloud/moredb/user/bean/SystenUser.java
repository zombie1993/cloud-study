package com.syl.springcloud.moredb.user.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统用户表 POJO
 *
 * @author syl
 * @create 2018-08-28
 *
 **/
public class SystenUser implements Serializable {
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String HEAD_PORTRAITS_URL = "head_portraits_url";
    public static final String STATE = "state";
    public static final String IS_DELETE = "is_delete";
    public static final String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String REMARK = "remark";

    public SystenUser(){}

    /**
     *
     */
    private String id;
    /**
     *
     */
    private String name;
    /**
     * 密码
     */
    private String password;
    /**
     * 头像url
     */
    private String headPortraitsUrl;
    /**
     * 状态
     */
    private Short state;
    /**
     * 是否删除
     */
    private Boolean delete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 备注
     */
    private String remark;

    /**
     *  get
     */
    public String getId() {
        return id;
    }

    /**
     *  set
     */
    public SystenUser setId(String id) {
        this.id = id;
        return this;
    }

    /**
     *  get
     */
    public String getName() {
        return name;
    }

    /**
     *  set
     */
    public SystenUser setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 密码 get
     */
    public String getPassword() {
        return password;
    }

    /**
     * 密码 set
     */
    public SystenUser setPassword(String password) {
        this.password = password;
        return this;
    }

    /**
     * 头像url get
     */
    public String getHeadPortraitsUrl() {
        return headPortraitsUrl;
    }

    /**
     * 头像url set
     */
    public SystenUser setHeadPortraitsUrl(String headPortraitsUrl) {
        this.headPortraitsUrl = headPortraitsUrl;
        return this;
    }

    /**
     * 状态 get
     */
    public Short getState() {
        return state;
    }

    /**
     * 状态 set
     */
    public SystenUser setState(Short state) {
        this.state = state;
        return this;
    }

    /**
     * 是否删除 get
     */
    public Boolean isDelete() {
        return delete;
    }

    /**
     * 是否删除 set
     */
    public SystenUser setDelete(Boolean delete) {
        this.delete = delete;
        return this;
    }

    /**
     * 创建时间 get
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间 set
     */
    public SystenUser setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    /**
     * 更新时间 get
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间 set
     */
    public SystenUser setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    /**
     * 备注 get
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注 set
     */
    public SystenUser setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("SystenUser{");
        sb.append("id=").append(id);
        sb.append("name=").append(name);
        sb.append("password=").append(password);
        sb.append("headPortraitsUrl=").append(headPortraitsUrl);
        sb.append("state=").append(state);
        sb.append("delete=").append(delete);
        sb.append("createTime=").append(createTime);
        sb.append("updateTime=").append(updateTime);
        sb.append("remark=").append(remark);
        sb.append("}");
        return sb.toString();
    }

}
