package com.syl.springcloud.consul.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

/**
 * @author syl
 * @create 2018-07-19 15:12
 **/
@RefreshScope
@RestController
public class ConsulController {
    @Value("${value}")
    private String value;
    @Value("${test}")
    private String test; // 测试多组配置文件

    @RequestMapping("/value")
    public String home() {
        return "config 1 value:" + value +" config 2 value:"+ test;
    }

//    @RequestMapping("/login")
//    public String login(){
//        return "login page";
//    }

}
