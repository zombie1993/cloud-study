package com.syl.springcloud.moredb.user.constants;

/**
 * @author syl
 * @create 2018-06-27 14:59
 **/
public class BaseConstant {
    public static final String CRUD_ID = "id";
    /**
     * 统一dao参数对象名
     */
    public static final String CRUD_BEAN = "bean";
    public static final String CRUD_CRITERIA = "criteria";
    public static final String CRUD_VALUE_LIST = "valueList";
    public static final String CRUD_COLUMN_VALUE = "columnValue";

}
