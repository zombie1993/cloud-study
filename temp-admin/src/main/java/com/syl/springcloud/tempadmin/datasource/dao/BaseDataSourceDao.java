package com.syl.springcloud.tempadmin.datasource.dao;

import com.syl.springcloud.tempadmin.datasource.bean.Schema;
import com.syl.springcloud.tempadmin.datasource.bean.SchemaColumns;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author syl
 * @create 2018-09-02 18:06
 **/
@SuppressWarnings("all")
public interface BaseDataSourceDao {

    /**
     * 新增表
     * @param schema
     * @return
     */
    int insertTable(Schema schema,List<SchemaColumns> detailList);

    /**
     * 查询所有表
     * @return
     */
    List<Schema> selectTableNameList(String schema);

    /**
     * 查询某个表的所有字段
     * @return
     */
    List<SchemaColumns> selectTableColumnList(@Param("schema") String schema, @Param("tableName") String tableName);

    
}
