package com.syl.springcloud.parent;

import com.syl.springcloud.parent.config.BaseControllerConfig;
import org.springframework.context.annotation.Import;

/**
 * @author syl
 * @create 2018-07-26 16:48
 **/
@Import({
    BaseControllerConfig.class
})
public class ParentAutoConfigure {
}
