package com.syl.springcloud.authorization.controller;

import com.syl.springcloud.authorization.bean.ReturnMessage;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.syl.springcloud.authorization.constant.LoginConstant.PUBLIC_KEY;

/**
 * 其实是ras
 * @author syl
 * @create 2018-08-15 14:35
 **/
@RestController
@RequestMapping("aes")
public class PublicKeyController {

    /**
     * 其实是获取ras 的公钥
     * @return
     */
    @RequestMapping("/get/secret/key")
    public ReturnMessage getKey(){
        return new ReturnMessage(200,PUBLIC_KEY);
    }

}
