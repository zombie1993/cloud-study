package com.syl.springcloud.tempadmin.entity.service;

import com.syl.framework.common.BaseService;
import com.syl.springcloud.tempadmin.entity.bean.EntityFiled;
import com.syl.springcloud.tempadmin.entity.bean.EntityFiledDTO;

import java.util.List;

/**
 * 实体字段属性 Service
 *
 * @author syl
 * @create 2018-09-02
 *  
 **/
public interface EntityFiledService extends BaseService<EntityFiled,EntityFiledDTO> {

    /**
     * 依据实体id 查询 所有字段
     * @param entityId
     * @return
     */
    List<EntityFiled> queryEntityFiled(String entityId);

}