### spring cloud study
该项目为spring cloud学习使用

### 项目介绍
0. cloud-study 整体父模块 项目依赖,依赖版本管理
1. authorization-center 授权认证中心
2. cloud-parent 基础parent-starter
3. config-centre 使用git方式的配置中心
4. config-centre-jdbc 使用数据库方式的配置中心
5. consul-test, consul-test2 主要测试客户端
6. my-one-starter 第一个starter应用
7. more-datasource-test 多数据源测试客户端
8. mybatis-starter 多数据源的starter
9. cloud-my-starter-remote-config 远程配置的starter
10. cloud-my-starter-security 授权中心客户端的starter
11. global-configs 用于存储 授权中心 和 cloud-my-starter-security 的安全密钥全局配置

### 使用须知
0. 需安装[consul](https://www.consul.io/downloads.html) 服务 使用 ./consul agent -dev -ui 启动服务
1. 需下载[JCE](http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html)
然后解压到${JDK_PATH}\jre\lib\security
2. 启动配置中心后访问[Test](127.0.0.1:17001/config/encrypt/status) 测试JCE是否正常 

### 注意事项
0. 除了cloud-study最外层的pom文件,其余pom文件一律不能写版本号,版本由它管理
1. 有README的项目一定要看README

### 启动顺序及准备
0. consul 服务 [consul界面地址](http://127.0.0.1:8500/ui)
1. redis 服务
3. mysql 服务 并初始化 sql包下的sql文件
4. config-centre-jdbc 
5. authorization-center 
6. 可以启动服务端愉快玩耍了