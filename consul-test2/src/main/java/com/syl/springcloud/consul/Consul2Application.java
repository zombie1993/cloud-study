package com.syl.springcloud.consul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author syl
 * @create 2018-07-19 15:11
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class Consul2Application {

    public static void main(String[] args) {
        SpringApplication.run(Consul2Application.class, args);
    }

}
