package com.syl.springcloud.moredb.user.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统用户表 DTO
 *
 * @author syl
 * @create 2018-08-28
 *
 **/
public class SystenUserDTO implements Serializable {

    public SystenUserDTO(){}

    /**
     *
     */
    private String id;
    /**
     *
     */
    private String name;
    /**
     * 密码
     */
    private String password;
    /**
     * 头像url
     */
    private String headPortraitsUrl;
    /**
     * 状态
     */
    private Short state;
    /**
     * 是否删除
     */
    private Boolean delete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 备注
     */
    private String remark;
    /**
     * 按create_time的开始时间查找
     */
    private Date createTimeStart;
    /**
     * 按create_time的截止时间查找
     */
    private Date createTimeEnd;
    /**
     * 按update_time的开始时间查找
     */
    private Date updateTimeStart;
    /**
     * 按update_time的截止时间查找
     */
    private Date updateTimeEnd;

    /**
     *  get
     */
    public String getId() {
        return id;
    }

    /**
     *  set
     */
    public SystenUserDTO setId(String id) {
        this.id = id;
        return this;
    }

    /**
     *  get
     */
    public String getName() {
        return name;
    }

    /**
     *  set
     */
    public SystenUserDTO setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 密码 get
     */
    public String getPassword() {
        return password;
    }

    /**
     * 密码 set
     */
    public SystenUserDTO setPassword(String password) {
        this.password = password;
        return this;
    }

    /**
     * 头像url get
     */
    public String getHeadPortraitsUrl() {
        return headPortraitsUrl;
    }

    /**
     * 头像url set
     */
    public SystenUserDTO setHeadPortraitsUrl(String headPortraitsUrl) {
        this.headPortraitsUrl = headPortraitsUrl;
        return this;
    }

    /**
     * 状态 get
     */
    public Short getState() {
        return state;
    }

    /**
     * 状态 set
     */
    public SystenUserDTO setState(Short state) {
        this.state = state;
        return this;
    }

    /**
     * 是否删除 get
     */
    public Boolean isDelete() {
        return delete;
    }

    /**
     * 是否删除 set
     */
    public SystenUserDTO setDelete(Boolean delete) {
        this.delete = delete;
        return this;
    }

    /**
     * 创建时间 get
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间 set
     */
    public SystenUserDTO setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    /**
     * 更新时间 get
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间 set
     */
    public SystenUserDTO setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    /**
     * 备注 get
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注 set
     */
    public SystenUserDTO setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    /**
     * 按create_time的开始时间查找 get
     */
    public Date getCreateTimeStart() {
        return createTimeStart;
    }

    /**
     * 按create_time的开始时间查找 set
     */
    public SystenUserDTO setCreateTimeStart(Date createTimeStart) {
        this.createTimeStart = createTimeStart;
        return this;
    }

    /**
     * 按create_time的截止时间查找 get
     */
    public Date getCreateTimeEnd() {
        return createTimeEnd;
    }

    /**
     * 按create_time的截止时间查找 set
     */
    public SystenUserDTO setCreateTimeEnd(Date createTimeEnd) {
        this.createTimeEnd = createTimeEnd;
        return this;
    }

    /**
     * 按update_time的开始时间查找 get
     */
    public Date getUpdateTimeStart() {
        return updateTimeStart;
    }

    /**
     * 按update_time的开始时间查找 set
     */
    public SystenUserDTO setUpdateTimeStart(Date updateTimeStart) {
        this.updateTimeStart = updateTimeStart;
        return this;
    }

    /**
     * 按update_time的截止时间查找 get
     */
    public Date getUpdateTimeEnd() {
        return updateTimeEnd;
    }

    /**
     * 按update_time的截止时间查找 set
     */
    public SystenUserDTO setUpdateTimeEnd(Date updateTimeEnd) {
        this.updateTimeEnd = updateTimeEnd;
        return this;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("SystenUserDTO{");
        sb.append("id=").append(id);
        sb.append("name=").append(name);
        sb.append("password=").append(password);
        sb.append("headPortraitsUrl=").append(headPortraitsUrl);
        sb.append("state=").append(state);
        sb.append("delete=").append(delete);
        sb.append("createTime=").append(createTime);
        sb.append("updateTime=").append(updateTime);
        sb.append("remark=").append(remark);
        sb.append("createTimeStart=").append(createTimeStart);
        sb.append("createTimeEnd=").append(createTimeEnd);
        sb.append("updateTimeStart=").append(updateTimeStart);
        sb.append("updateTimeEnd=").append(updateTimeEnd);
        sb.append("}");
        return sb.toString();
    }

}
