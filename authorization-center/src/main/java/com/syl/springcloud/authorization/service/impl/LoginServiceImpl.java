package com.syl.springcloud.authorization.service.impl;

import com.syl.springcloud.authorization.bean.DoubleResult;
import com.syl.springcloud.authorization.bean.UserBean;
import com.syl.springcloud.authorization.service.LoginService;
import org.apache.commons.lang.StringUtils;
import org.patchca.color.SingleColorFactory;
import org.patchca.filter.FilterFactory;
import org.patchca.filter.predefined.*;
import org.patchca.font.RandomFontFactory;
import org.patchca.service.ConfigurableCaptchaService;
import org.patchca.word.RandomWordFactory;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.Random;

/**
 * @author syl
 * @create 2018-08-07 10:09
 **/
@Service
public class LoginServiceImpl implements LoginService {

    @Override
    public DoubleResult<Integer, String> verifyCaptcha(String inputCode, String captcha) {
        if(StringUtils.isBlank(inputCode))return new DoubleResult<>(10,"请您输入验证码！");
        if(!captcha.toUpperCase().equals(inputCode.trim().toUpperCase()))return new DoubleResult<>(20,"您输入的验证码不正确！");
        return new DoubleResult<>(0,"");
    }

    @Override
    public ConfigurableCaptchaService initCaptchaConfig(int width, int height, boolean single) {
        ConfigurableCaptchaService cs = new ConfigurableCaptchaService();
        Random random = new Random();
        if(single)cs.setColorFactory(new SingleColorFactory(new Color(25, 60, 170)));
        else {
            cs.setColorFactory(x1 -> {
                int[] c = new int[3];
                int i = random.nextInt(c.length);
                for (int fi = 0; fi < c.length; fi++) {
                    if (fi == i) {
                        c[fi] = random.nextInt(71);
                    } else {
                        c[fi] = random.nextInt(256);
                    }
                }
                return new Color(c[0], c[1], c[2]);
            });
        }
        cs.setFilterFactory(selectFilter(cs));
        RandomFontFactory ff = new RandomFontFactory();
        ff.setMinSize(height <= 30 ? 30 : height-20);
        ff.setMaxSize(height <= 30 ? 40 : height-10);
        RandomWordFactory rwf = new RandomWordFactory();//验证码数量
        rwf.setMinLength(4);
        rwf.setMaxLength(6);
        cs.setWordFactory(rwf);
        cs.setFontFactory(ff);
        cs.setWidth(width <= 120 ? 120 : width);
        cs.setHeight(height <= 30 ? 40 : height);
        return cs;
    }

    private FilterFactory selectFilter(ConfigurableCaptchaService cs){
        int i = new Random().nextInt(5);
        switch (i){
            case 0:
                return new CurvesRippleFilterFactory(cs.getColorFactory());
            case 1:
                return new MarbleRippleFilterFactory();
            case 2:
                return new DoubleRippleFilterFactory();
            case 3:
                return new WobbleRippleFilterFactory();
            case 4:
                return new DiffuseRippleFilterFactory();
        }
        return new CurvesRippleFilterFactory(cs.getColorFactory());
    }

}
