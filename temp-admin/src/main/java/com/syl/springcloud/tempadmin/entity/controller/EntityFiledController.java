package com.syl.springcloud.tempadmin.entity.controller;

import com.syl.springcloud.tempadmin.entity.bean.EntityFiled;
import com.syl.springcloud.tempadmin.entity.service.EntityFiledService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *
 * @author syl
 * @create 2018-09-02 22:53
 **/
@RestController
@RequestMapping("/entity/filed")
public class EntityFiledController {

    @Autowired
    private EntityFiledService entityFiledService;

    @RequestMapping("/get/{entityId}")
    public String get(@PathVariable String entityId){
        if(StringUtils.isEmpty(entityId))return "error entity id not null";
        List<EntityFiled> list = entityFiledService.queryEntityFiled(entityId);
        for (EntityFiled entityFiled : list) {
            System.out.println(entityFiled);
        }
        return "ok";
    }

    @RequestMapping("/add/{entityId}")
//    @PostMapping("/save"/)
    public String testArr(String[] ids){
        System.out.println(ids.length);
        return "ok";
    }

}
