package com.syl.springcloud.authorization.enums;

import com.google.gson.annotations.SerializedName;

/**
 * 登录类型枚举
 * @author syl
 * @create 2018-08-07 11:23
 **/
public enum LoginTypeEnum {
    @SerializedName("1")
    PASSWORD(1, "密码模式"),
    @SerializedName("2")
    PASSWORD_IMAGE_CAPTCHA(2, "密码模式+图形验证码", "REG_VAL_CODE"),
    @SerializedName("3")
    SMS_CODE(3, "短信验证码模式"),
    @SerializedName("4")
    EMAIL(4, "邮箱账号+密码模式"),
    @SerializedName("5")
    WEI_CHAT_OAC(5, "微信公众号"),
    @SerializedName("6")
    WEI_CHAT_APPLET(6, "微信小程序"),
    NULL(null, null);

    /**  header 1 | code | name
     *  ---|---|---
     *  PASSWORD| 1 | 密码模式
     *  PASSWORD_IMAGE_CAPTCHA | 2 | 密码模式+图形验证码
     *  SMS_CODE | 3 | 短信验证码模式
     *  EMAIL| 4 | 邮箱账号+密码模式
     *  WEICHAT_OAC| 4 | 微信公众号
     *  WEICHAT_APPLET| 5 | 微信小程序
     **/
    private Integer code;
    private String name;
    private String sessionKey;

    LoginTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    LoginTypeEnum(Integer code, String name, String sessionKey) {
        this.code = code;
        this.name = name;
        this.sessionKey = sessionKey;
    }

    public static LoginTypeEnum getEnumByCode(Integer code) {
        LoginTypeEnum[] values = LoginTypeEnum.values();
        for (LoginTypeEnum em : values) {
            if (em.getCode() == code) return em;
        }
        return NULL;
    }

    public static LoginTypeEnum getEnumByName(String name) {
        LoginTypeEnum[] values = LoginTypeEnum.values();
        for (LoginTypeEnum em : values) {
            if (em.getName() == name) return em;
        }
        return NULL;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getSessionKey() {
        return sessionKey;
    }
}
