package com.syl.springcloud.security.config.util;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * @author syl
 * @create 2018-08-02 11:13
 **/
public class CommonUtils {

    /**
     * 从指定全局配置文件夹中读取配置文件
     * @param configName 配置名称
     * @return
     */
    public static Properties getFileProperties(String configName){
        String userDir = System.getProperty("user.dir");
        StringBuffer sb = new StringBuffer(userDir);
        sb.append(File.separator)
        .append("global-configs")
        .append(File.separator)
        .append(configName);
        Properties props = new Properties();
        InputStreamReader inputStreamReader;
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(sb.toString()));
            inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
            props.load(inputStreamReader);
            return props;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *  从配置文件中读取String list (list字符串以,号分隔)
     * @param properties
     * @param key
     * @return
     */
    public static List<String> getPropertiesList(Properties properties, String key){
        String property = properties.getProperty(key);
        String[] split = property.split(",");
        if(split == null)return new ArrayList<>();
        return Arrays.asList(split);
    }

}
