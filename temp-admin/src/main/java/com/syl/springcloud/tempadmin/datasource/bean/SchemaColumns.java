package com.syl.springcloud.tempadmin.datasource.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 数据库表
 *
 * @author syl
 * @create 2018-09-02 20:41
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class SchemaColumns extends Schema{
    /**
     * 列名
     */
    private String columnName;
    /**
     * 列类型
     */
    private String columnType;
    /**
     * 主键
     */
    private boolean primaryKey;
    /**
     * 列注释
     */
    private String columnComment;

    public SchemaColumns() {}

    public SchemaColumns(String columnName, String columnType, boolean primaryKey, String columnComment) {
        this.columnName = columnName;
        this.columnType = columnType;
        this.primaryKey = primaryKey;
        this.columnComment = columnComment;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = "PRI".equals(primaryKey);
    }
}
