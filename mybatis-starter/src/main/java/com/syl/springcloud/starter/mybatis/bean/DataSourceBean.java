package com.syl.springcloud.starter.mybatis.bean;

/**
 *
 * @author syl
 * @create 2018-08-29 20:16
 **/
public class DataSourceBean {
    private String url;
    private String driverClassName;
    private String username;
    private String password;

    public String getUrl() {
        return url;
    }

    public DataSourceBean setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public DataSourceBean setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public DataSourceBean setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public DataSourceBean setPassword(String password) {
        this.password = password;
        return this;
    }
}
