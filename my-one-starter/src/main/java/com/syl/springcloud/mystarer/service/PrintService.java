package com.syl.springcloud.mystarer.service;

/**
 * @author syl
 * @create 2018-07-26 16:45
 **/
public class PrintService {
    private String value;

    public PrintService(String value) {
        this.value = value;
    }

    public void print(){
        System.out.println(this.value);
    }

}
