//package com.syl.springcloud.tempadmin.datasource.service;
//
//import java.util.List;
//import javax.sql.DataSource;
//
//import com.syl.framework.common.enums.PrimaryKeyType;
//import com.syl.springcloud.starter.mybatis.enums.DataSourceEnum;
//import com.syl.springcloud.tempadmin.datasource.bean.Schema;
//import com.syl.springcloud.tempadmin.datasource.bean.SchemaColumns;
//
///**
// * @author syl
// * @create 2018-09-02 15:02
// **/
//public interface SchemaTableService {
//
//    /**
//     * 创建表(真正创建)
//     * @param schema schema tableName tableComment 必填
//     * @param primaryKeyType 主键类型
//     * @param sourceEnum 数据源枚举
//     */
//    boolean createTable(Schema schema, PrimaryKeyType primaryKeyType, DataSourceEnum sourceEnum);
//
//    /**
//     * 创建表(真正创建)
//     * @param schema schema tableName tableComment 必填
//     * @param primaryKeyType 主键类型
//     * @param sourceEnum 数据源枚举
//     * @param columnsList 初始列
//     */
//    boolean createTable(Schema schema, PrimaryKeyType primaryKeyType, DataSourceEnum sourceEnum, List<SchemaColumns> columnsList);
//
////    boolean saveTable(Schema schema);
//
//    /**
//     * 查询数据源下所有表
//     * @param sourceEnum 数据源枚举
//     * @param dataSource 数据源连接
//     * @return
//     */
//    List<Schema> selectAllTable(DataSourceEnum sourceEnum, DataSource dataSource);
//
//    /**
//     * 查询数据源下某个表中所有列
//     * @param tableName 表名
//     * @param sourceEnum 数据源枚举
//     * @param dataSource 数据源连接
//     * @return
//     */
//    List<SchemaColumns> selectAllColumn(String tableName, DataSourceEnum sourceEnum, DataSource dataSource);
//}
