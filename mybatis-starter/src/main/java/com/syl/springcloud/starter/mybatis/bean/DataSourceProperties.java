package com.syl.springcloud.starter.mybatis.bean;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.LinkedHashMap;

/**
 * my-datasource 属性映射bean
 * @author syl
 * @create 2018-08-29 20:00
 **/
@ConfigurationProperties("my-datasource")
public class DataSourceProperties {
    /**
     * 数据源连接  配置在第一个的数据源为默认数据源
     */
    private LinkedHashMap<String,DataSourceBean> connect;
    private Integer maxIdleConnection;
    private Integer maxActiveConnection;
    private Boolean healthCheck;
    private Integer healthCheckIntervalSecond;

    public LinkedHashMap<String, DataSourceBean> getConnect() {
        return connect;
    }

    public DataSourceProperties setConnect(LinkedHashMap<String, DataSourceBean> connect) {
        this.connect = connect;
        return this;
    }

    public Integer getMaxIdleConnection() {
        return maxIdleConnection;
    }

    public DataSourceProperties setMaxIdleConnection(Integer maxIdleConnection) {
        this.maxIdleConnection = maxIdleConnection;
        return this;
    }

    public Integer getMaxActiveConnection() {
        return maxActiveConnection;
    }

    public DataSourceProperties setMaxActiveConnection(Integer maxActiveConnection) {
        this.maxActiveConnection = maxActiveConnection;
        return this;
    }

    public Boolean getHealthCheck() {
        return healthCheck;
    }

    public DataSourceProperties setHealthCheck(Boolean healthCheck) {
        this.healthCheck = healthCheck;
        return this;
    }

    public Integer getHealthCheckIntervalSecond() {
        return healthCheckIntervalSecond;
    }

    public DataSourceProperties setHealthCheckIntervalSecond(Integer healthCheckIntervalSecond) {
        this.healthCheckIntervalSecond = healthCheckIntervalSecond;
        return this;
    }

    @Override
    public String toString() {
        return "DataSourceProperties{" +
                "connect=" + connect +
                ", maxIdleConnection=" + maxIdleConnection +
                ", maxActiveConnection=" + maxActiveConnection +
                ", healthCheck=" + healthCheck +
                ", healthCheckIntervalSecond=" + healthCheckIntervalSecond +
                '}';
    }
}
