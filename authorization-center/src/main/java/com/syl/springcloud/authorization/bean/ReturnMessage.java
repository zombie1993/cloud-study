package com.syl.springcloud.authorization.bean;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author syl
 * @create 2018-08-03 15:04
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class ReturnMessage {
    private int status;
    private String message;
    private Object data;
    private long timestamp;

    public ReturnMessage(int status, String message) {
        this(status,message,null);
    }

    public ReturnMessage(int status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
        this.timestamp = System.currentTimeMillis();
    }

    public String toJson(){
        return new Gson().toJson(this);
    }
}
