package com.syl.springcloud.parent.listener;

import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;

import java.util.HashMap;
import java.util.Map;

/**
 * @author syl
 * @create 2018-08-01 17:04
 **/
public class MyBaseConfigListener implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {

    @Override
    public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
        ConfigurableEnvironment environment = event.getEnvironment();
        MutablePropertySources propertySources = environment.getPropertySources();
        String contextPath = environment.getProperty("server.servlet.context-path");
        Map<String, Object> myMap = new HashMap();
        if(null != contextPath && !contextPath.isEmpty()){
            myMap.put("spring.cloud.consul.discovery.healthCheckPath", contextPath+"/");
        }
        propertySources.addFirst(new MapPropertySource("myBaseConfig", myMap));
    }

}
