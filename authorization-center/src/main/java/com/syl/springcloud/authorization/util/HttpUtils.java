package com.syl.springcloud.authorization.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @author syl
 * @create 2018-08-10 16:01
 **/
@Component
public class HttpUtils {
    private RestTemplate restTemplate;

    public HttpUtils(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getForString(String url, Map<String,String> params, Map<String,String> header) throws RestClientException {
        HttpHeaders headers = new HttpHeaders();
        if(header != null){
            for (String key : header.keySet()) {
                headers.add(key,header.get(key));
            }
        }
        return getForString(url,params,headers);
    }

    public String getForString(String url, Map<String,String> params,HttpHeaders headers) throws RestClientException {
        return getForString(url,params, new HttpEntity<>(headers));
    }

    public String getForString(String url, Map<String,String> params, HttpEntity<?> header) throws RestClientException {
        StringBuffer sb = new StringBuffer(url);
        if(params != null){
            Set<String> keySet = params.keySet();
            if(keySet.size() < 2){
                String key = keySet.iterator().next();
                sb.append("?").append(key).append("=").append(params.get(key));
            }
            else{
                Iterator<String> iterator = keySet.iterator();
                int i = 0;
                while (iterator.hasNext()){
                    String key = iterator.next();
                    if(i == 0)sb.append("?").append(key).append("=").append(params.get(key));
                    else sb.append("&").append(key).append("=").append(params.get(key));
                    i++;
                }
            }
        }
        return getForString(sb.toString(),header);
    }

    /**
     * 以get请求获取内容
     * @param url
     * @param header
     * @return
     */
    public String getForString(String url, HttpEntity<?> header) throws RestClientException {
        if(header == null){
            return restTemplate.getForObject(url, String.class);
        }else{
            ResponseEntity<String> entity = restTemplate.exchange(url, HttpMethod.GET, header, String.class);
            return entity.getBody();
        }
    }

    /**
     * 以post请求获取内容 发送json参数
     * @param url
     * @param json
     * @param header
     * @return
     */
    public String postForString(String url,String json, HttpEntity<?> header){

        return "";
    }


//    public String postForString(String url,Map<String,String> param){
//        return postForObject(url,param,String.class,new HttpEntity<>(null));
//    }
//
//    public <T> T postForObject(String url,Class<T> responseType,Map<String,String> param){
//        return postForObject(url,param,responseType,new HttpEntity<>(null));
//    }
//
//    /**
//     * 以post请求获取内容 发送表单数据
//     * @param url
//     * @param param
//     * @param header
//     * @return
//     */
//    public String postForString(String url,Map<String,String> param, Map<String,String> header){
//        HttpHeaders headers = new HttpHeaders();
//        for (String key : header.keySet()) {
//            headers.add(key,header.get(key));
//        }
//        return postForString(url,param,new HttpEntity<>(headers));
//    }


    /**
     * 以post请求获取内容 发送表单数据
     * @param url
     * @param param
     * @param headers
     * @return
     */
    public <T> T postForObject(String url,Map<String,String> param,Class<T> responseType, HttpHeaders headers) throws RestClientException{
        if(headers == null){
            return null;
        }else {
            MultiValueMap<String, String> map= new LinkedMultiValueMap();
            Set<String> keySet = param.keySet();
            for (String key : keySet) {
                map.add(key,param.get(key));
            }
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
            return restTemplate.postForObject(url,request,responseType);
        }
    }

    /**
     * 以post请求获取内容 发送表单数据
     * @param url
     * @param param
     * @param responseType
     * @param header
     * @param <T>
     * @return
     */
//    public <T> T postForObject(String url,Map<String,String> param,Class<T> responseType, HttpEntity<?> header){
//        MultiValueMap<String, String> map= new LinkedMultiValueMap();
//        map.add("email", "first.last@example.com");
//
//        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
//
//        if(header == null){
//            return restTemplate.postForObject(url,param,responseType);
//        }else {
////            if(param != null){
////                Set<String> keySet = param.keySet();
////                for (String key : keySet) {
////                    HttpHeaders headers = header.getHeaders();
////                    headers.add(key,param.get(key));
////                }
////            }
//            //ResponseEntity<T> exchange = restTemplate.exchange(url, HttpMethod.POST, header, responseType,param);
//            return restTemplate.postForObject(url,param,responseType);
//        }
//    }

}
