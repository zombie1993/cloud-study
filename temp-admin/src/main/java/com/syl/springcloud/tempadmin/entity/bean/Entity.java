package com.syl.springcloud.tempadmin.entity.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * 实体 POJO
 *
 * @author syl
 * @create 2018-09-03
 *
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class Entity implements Serializable {
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String BUSINESS_ID = "business_id";
    public static final String BUSINESS_CODE = "business_code";
    public static final String TABLE_NAME = "table_name";
    public static final String DATASOURCE = "datasource";
    public static final String TYPE = "type";
    public static final String SOURCE = "source";
    public static final String IS_DELETE = "is_delete";
    public static final String IS_SYNC = "is_sync";
    public static final String IS_CONFIG = "is_config";
    public static final String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String REMARK = "remark";

    public Entity(){}

    /**
     * id
     */
    private String id;
    /**
     * 实体名称
     */
    private String name;
    /**
     * 业务id
     */
    private String businessId;
    /**
     * 业务编码
     */
    private String businessCode;
    /**
     * 数据表名称
     */
    private String tableName;
    /**
     * 数据源
     */
    private String datasource;
    /**
     * 实体类型
     */
    private Integer type;
    /**
     * 来源
     */
    private Integer source;
    /**
     * 是否删除
     */
    private Boolean delete;
    /**
     * 是否同步
     */
    private Boolean sync;
    /**
     * 是否配置
     */
    private Boolean config;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 备注
     */
    private String remark;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity entity = (Entity) o;
        return Objects.equals(tableName, entity.tableName) && Objects.equals(datasource, entity.datasource);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tableName, datasource);
    }
}
