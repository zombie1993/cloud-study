/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : iadmin

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 09/08/2018 17:54:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for system_config
-- ----------------------------
DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '名称',
  `key` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'key',
  `value` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'value',
  `visibility` bit(1) NULL DEFAULT b'1' COMMENT '是否可见',
  `enabled` bit(1) NULL DEFAULT b'1' COMMENT '是否启用',
  `application` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '应用',
  `profile` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '描述',
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'master' COMMENT '标签',
  `create_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_config
-- ----------------------------
INSERT INTO `system_config` VALUES (1, 'test', 'test', 'hello world', b'1', b'1', 'consul-config-jdbc', 'test', 'master', '2018-07-24 11:39:14', NULL, NULL);
INSERT INTO `system_config` VALUES (2, 'value 密码1234', 'value', '{cipher}f84e0efb1bcc60f803bcf2881c00f117bcc60c8a67c9a0ee1d54898d1ee6767b', b'1', b'1', 'consul-config-jdbc', 'test', 'master', '2018-07-24 11:39:17', NULL, NULL);
INSERT INTO `system_config` VALUES (3, 'port', 'server.port', '18888', b'0', b'0', 'consul-config-jdbc', 'test', 'master', '2018-08-02 16:46:29', NULL, NULL);
INSERT INTO `system_config` VALUES (4, 'actuator暴露的接口', 'management.endpoints.web.exposure.include', 'refresh', b'0', b'1', 'consul-config-jdbc', 'test', 'master', '2018-07-25 16:50:15', NULL, 'spring boot 2.0后需主动配置暴露接口');
INSERT INTO `system_config` VALUES (5, 'context path', 'server.servlet.context-path', '/test', b'0', b'0', 'consul-config-jdbc', 'test', 'master', '2018-08-02 10:03:29', NULL, NULL);
INSERT INTO `system_config` VALUES (6, 'consul host', 'spring.cloud.consul.host', '127.0.0.1', b'0', b'0', 'consul-config-jdbc', 'test', 'master', '2018-08-02 16:46:35', NULL, NULL);
INSERT INTO `system_config` VALUES (7, 'consul port', 'spring.cloud.consul.port', '8500', b'0', b'0', 'consul-config-jdbc', 'test', 'master', '2018-08-02 16:46:48', NULL, NULL);
INSERT INTO `system_config` VALUES (8, '心跳地址', 'spring.cloud.consul.discovery.healthCheckPath', '/test/', b'0', b'0', 'consul-config-jdbc', 'test', 'master', '2018-08-02 09:54:24', NULL, NULL);
INSERT INTO `system_config` VALUES (9, '心跳频率', 'spring.cloud.consul.discovery.healthCheckInterval', '5s', b'0', b'0', 'consul-config-jdbc', 'test', 'master', '2018-08-02 16:46:51', NULL, '心跳频率必须带单位s/m/h');
INSERT INTO `system_config` VALUES (10, '环境', 'spring.profiles.active', 'dev', b'1', b'1', 'consul-config-jdbc', 'test', 'master', '2018-07-25 17:33:08', NULL, NULL);
INSERT INTO `system_config` VALUES (11, '最简单配置', 'spring.cloud.consul.host', '127.0.0.1', b'0', b'1', 'simple-config-jdbc', 'test', 'master', '2018-07-31 11:25:04', NULL, NULL);
INSERT INTO `system_config` VALUES (12, '最简单配置', 'spring.cloud.consul.port', '8500', b'0', b'1', 'simple-config-jdbc', 'test', 'master', '2018-07-31 11:25:05', NULL, NULL);
INSERT INTO `system_config` VALUES (13, '最简单配置', 'spring.datasource.url', 'jdbc:mysql://127.0.0.1:3306/iadmin?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=true', b'0', b'1', 'simple-config-jdbc', 'test', 'master', '2018-07-31 14:20:42', NULL, NULL);
INSERT INTO `system_config` VALUES (14, '最简单配置', 'spring.datasource.driver-class-name', 'com.mysql.cj.jdbc.Driver', b'0', b'1', 'simple-config-jdbc', 'test', 'master', '2018-08-08 14:45:27', NULL, NULL);
INSERT INTO `system_config` VALUES (15, '最简单配置', 'spring.datasource.username', '{cipher}edcffbc676968e9aad97d62517484c246d0d2c07315828fa4b4bf40233464f71', b'0', b'1', 'simple-config-jdbc', 'test', 'master', '2018-07-31 14:20:46', NULL, NULL);
INSERT INTO `system_config` VALUES (16, '最简单配置', 'spring.datasource.password', '{cipher}cc50f7176c94cddc02e74c95f0a1d3ba5e1818e451e17067a640bfa6d8f5744d', b'0', b'1', 'simple-config-jdbc', 'test', 'master', '2018-07-31 14:20:50', NULL, NULL);
INSERT INTO `system_config` VALUES (18, 'test_config', 'test', '苟利国家生死以', b'1', b'1', 'config-test-more', 'test', 'master', '2018-08-02 15:44:31', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
