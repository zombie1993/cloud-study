package com.syl.springcloud.authorization.constant;

/**
 * @author syl
 * @create 2018-08-10 9:55
 **/
public class BaseConstant {

    public static final String SPLIT_SYMBOL = ";";

    public static final String TOKEN_CACHE = "access_token_cache";

    /**
     *匿名
     */
    public static final String ANONYMOUS_ROLE = "ROLE_ANONYMOUS";

    /**
     * 除了匿名外最少权限的角色
     */
    public static final String LOWLINESS_ROLE = "ROLE_USER";
}
