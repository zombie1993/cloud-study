package com.syl.springcloud.mystarer;

import com.syl.springcloud.mystarer.config.BaseControllerConfig;
import com.syl.springcloud.mystarer.config.StarterServiceProperties;
import com.syl.springcloud.mystarer.service.PrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

/**
 * @author syl
 * @create 2018-07-26 16:48
 **/
@ConditionalOnClass(PrintService.class)
@EnableConfigurationProperties(StarterServiceProperties.class)
@Import({
    BaseControllerConfig.class
})
public class AutoConfigure {
    @Autowired
    private StarterServiceProperties properties;

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "example.service", value = "enabled", havingValue = "true")
    PrintService starterService (){
        return new PrintService(properties.getConfig());
    }
}
