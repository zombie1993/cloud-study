package com.syl.springcloud.authorization.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author syl
 * @create 2018-08-10 10:50
 **/
@Configuration
public class BaseConfig {

    /**
     * 使用 RestTemplate
     * @return
     */
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
