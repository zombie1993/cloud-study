package com.syl.springcloud.configjdbc.bean;


import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author syl
 * @create 2018-07-19 15:48
 **/
@ConfigurationProperties()
public class UserInfo {

    private String value;

    public String getValue() {
        return value;
    }

    public UserInfo setValue(String value) {
        this.value = value;
        return this;
    }
}
