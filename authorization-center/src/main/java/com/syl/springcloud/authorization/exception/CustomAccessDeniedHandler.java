package com.syl.springcloud.authorization.exception;

import com.syl.springcloud.authorization.bean.ReturnMessage;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author syl
 * @create 2018-08-14 17:43
 **/
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        try {
            response.getWriter().println(new ReturnMessage(401,accessDeniedException.getMessage()).toJson());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
