package com.syl.springcloud.tempadmin.entity.enums;

/**
 *
 * 字段类型映射枚举
 *
 * @author syl
 * @create 2018-09-03 14:46
 *
 *  h | code | db_type_name | java_type_name  | ui_type_name | default_length
 *  - | - | - | - | - | -
 *  TEXT | 1 | varchar | String | text |
 *  EMAIL |2 | varchar | String | email | 50
 *  NUMBER | 3 | decimal | Double | number | 10
 *  PRECISE_NUMBER | 4 | decimal | java.math.BigDecimal| pnumber | 10
 *  PHONE | 5| varchar | String | phone | 20
 *  URL | 6 | varchar | String | url |
 *  SELECT | 7 | tinyint | Integer | select | 3
 *  ENTITY | 8 | varchar | String | entity | 40
 *  RADIO | 9 | bit | Boolean | radio | 1
 *  CHECKBOX | 10 | tinyint | Integer | checkbox | 3
 *  IMAGE | 11 | varchar | String | image | 200
 *  FILE | 12 | varchar | String | file | 200
 *  TIME | 13 | datetime | java.util.Date | datetime | 0
 *  DATE | 14 | date | java.sql.Date | date | 0
 *  SE_DATE | 15 | date | java.sql.Date | sedate | 0
 *  DATETIME | 16 | datetime | java.util.Date | datetime | 0
 *  SE_DATETIME | 17 | datetime | java.util.Date | sedatetime | 0
 **/
public enum EntityFiledTypeEnum {
    TEXT(1, "varchar", "String", "text", null),
    EMAIL(2, "varchar", "String", "email", 50),
    NUMBER(3, "decimal", "Double", "number", 10),
    PRECISE_NUMBER(4, "decimal", "java.math.BigDecimal", "pnumber", 10),
    PHONE(5, "varchar", "String", "phone", 20),
    URL(6, "varchar", "String", "url", null),
    SELECT(7, "tinyint", "Integer", "select", 3),
    ENTITY(8, "varchar", "String", "entity", 40),
    RADIO(9, "bit", "Boolean", "radio", 1),
    CHECKBOX(10, "tinyint", "Integer", "checkbox", 3),
    IMAGE(11, "varchar", "String", "image", 200),
    FILE(12, "varchar", "String", "file", 200),
    TIME(13, "datetime", "java.util.Date", "datetime", 0),
    DATE(14, "date", "java.sql.Date", "date", 0),
    SE_DATE(15, "date", "java.sql.Date", "sedate", 0),
    DATETIME(16, "datetime", "java.util.Date", "datetime", 0),
    SE_DATETIME(17, "datetime", "java.util.Date", "sedatetime", 0),
    NULL(null, null, null, null, null);

    private Integer code;
    private String dbTypeName;
    private String javaTypeName;
    private String uiTypeName;
    private Integer defaultLength;

    EntityFiledTypeEnum(Integer code, String dbTypeName, String javaTypeName, String uiTypeName, Integer defaultLength) {
        this.code = code;
        this.dbTypeName = dbTypeName;
        this.javaTypeName = javaTypeName;
        this.uiTypeName = uiTypeName;
        this.defaultLength = defaultLength;
    }

    public static EntityFiledTypeEnum getEnumByCode(Integer code) {
        EntityFiledTypeEnum[] values = EntityFiledTypeEnum.values();
        for (EntityFiledTypeEnum em : values) {
            Integer emCode = em.getCode();
            if (emCode == null) continue;
            if (emCode.equals(code)) return em;
        }
        return NULL;
    }

    public static EntityFiledTypeEnum getEnumByDbTypeName(String dbTypeName) {
        EntityFiledTypeEnum[] values = EntityFiledTypeEnum.values();
        for (EntityFiledTypeEnum em : values) {
            String emDbTypeName = em.getDbTypeName();
            if (emDbTypeName == null) continue;
            if (emDbTypeName.equals(dbTypeName)) return em;
        }
        return NULL;
    }

    public static EntityFiledTypeEnum getEnumByJavaTypeName(String javaTypeName) {
        EntityFiledTypeEnum[] values = EntityFiledTypeEnum.values();
        for (EntityFiledTypeEnum em : values) {
            String emJavaTypeName = em.getJavaTypeName();
            if (emJavaTypeName == null) continue;
            if (emJavaTypeName.equals(javaTypeName)) return em;
        }
        return NULL;
    }

    public static EntityFiledTypeEnum getEnumByUiTypeName(String uiTypeName) {
        EntityFiledTypeEnum[] values = EntityFiledTypeEnum.values();
        for (EntityFiledTypeEnum em : values) {
            String emUiTypeName = em.getUiTypeName();
            if (emUiTypeName == null) continue;
            if (emUiTypeName.equals(uiTypeName)) return em;
        }
        return NULL;
    }

    public static EntityFiledTypeEnum getEnumByDefaultLength(Integer defaultLength) {
        EntityFiledTypeEnum[] values = EntityFiledTypeEnum.values();
        for (EntityFiledTypeEnum em : values) {
            Integer emDefaultLength = em.getDefaultLength();
            if (emDefaultLength == null) continue;
            if (emDefaultLength.equals(defaultLength)) return em;
        }
        return NULL;
    }

    public Integer getCode() {
        return code;
    }

    public String getDbTypeName() {
        return dbTypeName;
    }

    public String getJavaTypeName() {
        return javaTypeName;
    }

    public String getUiTypeName() {
        return uiTypeName;
    }

    public Integer getDefaultLength() {
        return defaultLength;
    }
}
