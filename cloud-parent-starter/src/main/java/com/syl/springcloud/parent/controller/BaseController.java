package com.syl.springcloud.parent.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *  参考类
 *  ConfigServerAutoConfiguration 自动配置入口
 *  ConfigServerEncryptionConfiguration 配置文件类
 *  EncryptionController 实际功能控制器
 *
 * @author syl
 * @create 2018-07-26 16:00
 **/
@RestController
@RequestMapping(path = "${spring.cloud.config.server.prefix:}")
public class BaseController {

    @RequestMapping("/")
    public String health(){
        return "successful project launch";
    }

}
