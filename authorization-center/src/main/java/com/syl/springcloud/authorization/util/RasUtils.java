package com.syl.springcloud.authorization.util;

import org.apache.commons.codec.binary.Base64;
import sun.misc.BASE64Decoder;

import javax.crypto.Cipher;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * @author syl
 * @create 2018-08-15 14:47
 **/
public class RasUtils {
    /**
     * 把字符串转换为标准公钥
     * @param key 不能随便写
     * @throws Exception
     */
    private static PublicKey getPublicKey(String key) throws Exception {
        String str = key.replaceAll("\n","");
        byte[] keyBytes;
        keyBytes = (new BASE64Decoder()).decodeBuffer(str);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(keySpec);
        return publicKey;
    }

    /**
     * 把字符串转换为标准私钥
     * @param key 不能随便写
     * @throws Exception
     */
    private static PrivateKey getPrivateKey(String key) throws Exception {
        String str = key.replaceAll("\n","");
        byte[] keyBytes;
        keyBytes = (new BASE64Decoder()).decodeBuffer(str);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return privateKey;
    }

    //************************加密解密**************************

    /**
     * 加密
     * @param plainText
     * @param publicKey 公钥不能随便写
     * @return
     */
    public static String encrypt(String plainText,String publicKey){
        byte[] bytes;
        try {
            bytes = encrypt(plainText.getBytes(), publicKey);
            return Base64.encodeBase64String(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 加密
     * @param plainText
     * @param publicKey 公钥不能随便写
     * @return
     */
    public static byte[] encrypt(byte[] plainText,String publicKey) throws Exception {
        PublicKey publicKey_ = getPublicKey(publicKey);
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey_);
        byte[] bt_encrypted = cipher.doFinal(plainText);
        return bt_encrypted;
    }

    public static String decrypt(String encrypted,String privateKey){
        try {
            byte[] decrypt = decrypt(Base64.decodeBase64(encrypted), privateKey);
            return new String(decrypt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] decrypt(byte[] encrypted,String privateKey) throws Exception {
        PrivateKey privateKey_ = getPrivateKey(privateKey);
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey_);
        byte[] bt_original = cipher.doFinal(encrypted);
        return bt_original;
    }

}
