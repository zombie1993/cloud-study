package com.syl.springcloud.authorization;

import com.syl.springcloud.authorization.bean.AuthorizationConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @author syl
 * @create 2018-07-26 14:08
 **/
@EnableDiscoveryClient
@SpringBootApplication
@EnableResourceServer
@EnableConfigurationProperties({AuthorizationConfig.class})
@EnableRedisHttpSession
public class AuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }

}
