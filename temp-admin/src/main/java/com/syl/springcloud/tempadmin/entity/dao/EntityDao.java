package com.syl.springcloud.tempadmin.entity.dao;

import com.syl.framework.common.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import com.syl.springcloud.tempadmin.entity.bean.Entity;
import com.syl.springcloud.tempadmin.entity.bean.EntityDTO;

/**
* 基础实体表类 Dao
*
* @author syl
* @create 2018-09-02
*
**/
@Mapper
@Repository
public interface EntityDao extends BaseDao<Entity,EntityDTO> {
    //Business code

}
