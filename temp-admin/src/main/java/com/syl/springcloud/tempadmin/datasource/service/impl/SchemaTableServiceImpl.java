//package com.syl.springcloud.tempadmin.datasource.service.impl;
//
//import com.syl.springcloud.starter.mybatis.enums.DataSourceEnum;
//import com.syl.springcloud.tempadmin.datasource.bean.Schema;
//import com.syl.springcloud.tempadmin.datasource.bean.SchemaColumns;
//import com.syl.springcloud.tempadmin.datasource.dao.BaseDataSourceDao;
//import com.syl.springcloud.tempadmin.datasource.service.SchemaTableService;
//import com.syl.springcloud.tempadmin.entity.service.EntityService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.context.WebApplicationContext;
//import javax.sql.DataSource;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//import static com.syl.springcloud.tempadmin.datasource.constant.DaoMethodConstant.*;
//
///**
// * @author syl
// * @create 2018-09-02 15:02
// **/
//@Service
//public class SchemaTableServiceImpl implements SchemaTableService {
//    private static final Logger LOG = LoggerFactory.getLogger(SchemaTableServiceImpl.class);
//    @Autowired
//    private WebApplicationContext webApplicationContext;
//    @Autowired
//    private EntityService entityService;
//
//    private List<SchemaColumns> initDefaultColumn(Schema schema){
//        List<SchemaColumns> columnsList = new ArrayList<>(10);
//        columnsList.add(new SchemaColumns("id","varchar(40)", true, "主键id"));
//        columnsList.add(new SchemaColumns("name","varchar(20)", false, "名称"));
//        columnsList.add(new SchemaColumns("user_id","varchar(40)", false, "用户id"));
//        columnsList.add(new SchemaColumns("is_delete","bit", false, "是否删除"));
//        columnsList.add(new SchemaColumns("create_time","datetime", false, "创建时间"));
//        columnsList.add(new SchemaColumns("update_time","datetime", false, "更新时间"));
//        return columnsList;
//    }
//
//    @Override
//    @Transactional
//    public boolean createTable(Schema schema, PrimaryKeyType primaryKeyType, DataSourceEnum sourceEnum) {
//        return this.createTable(schema,primaryKeyType,sourceEnum,initDefaultColumn(schema));
//    }
//
//    @Override
//    public boolean createTable(Schema schema, PrimaryKeyType primaryKeyType, DataSourceEnum sourceEnum, List<SchemaColumns> columnsList) {
//        BaseDataSourceDao sourceDao = getDataSourceDao(sourceEnum);
//        try {
//            Method method = sourceDao.getClass().getMethod(INSERT_TABLE__METHOD_NAME, Schema.class,List.class);
//            boolean b1 = method.invoke(sourceDao, schema, columnsList) != null;
////            entityService.save()
//
//            return b1;
//        } catch (NoSuchMethodException e) {
//            LOG.error("反射method 方法异常",e);
//        }catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
//
//    @Override
//    public List<Schema> selectAllTable(DataSourceEnum sourceEnum, DataSource dataSource) {
//        BaseDataSourceDao dataSourceDao = getDataSourceDao(sourceEnum);
//        try {
//            Method method = dataSourceDao.getClass().getMethod(TABLE_METHOD_NAME, String.class);
//            String schema = dataSource.getConnection().getCatalog();
//            return (List<Schema>) method.invoke(dataSourceDao, schema);
//        } catch (NoSuchMethodException e) {
//            LOG.error("反射DataSourceDao 方法异常",e);
//        } catch (SQLException e) {
//            LOG.error("获取数据库连接失败",e);
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    @Override
//    public List<SchemaColumns> selectAllColumn(String tableName, DataSourceEnum sourceEnum, javax.sql.DataSource dataSource) {
//        BaseDataSourceDao dataSourceDao = getDataSourceDao(sourceEnum);
//        try {
//            Method method = dataSourceDao.getClass().getMethod(COLUMN_METHOD_NAME, String.class, String.class);
//            String schema = dataSource.getConnection().getCatalog();
//            Object invoke = method.invoke(dataSourceDao, schema, tableName);
//            return (List<SchemaColumns>) invoke;
//        } catch (NoSuchMethodException e) {
//            LOG.error("反射DataSourceDao 方法异常",e);
//        } catch (SQLException e) {
//            LOG.error("获取数据库连接失败",e);
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    /**
//     * 第一个字符小写
//     * @param string
//     * @return
//     */
//    private String firstCharLower(String string){
//        return String.valueOf(string.charAt(0)).toLowerCase() + string.substring(1);
//    }
//
//    private BaseDataSourceDao getDataSourceDao(DataSourceEnum sourceEnum){
//        String dbType = sourceEnum.getDbType();
//        return (BaseDataSourceDao) webApplicationContext.getBean(firstCharLower(dbType)+"DataSourceDao");
//    }
//
//}
