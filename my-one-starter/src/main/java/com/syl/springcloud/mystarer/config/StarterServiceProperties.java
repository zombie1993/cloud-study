package com.syl.springcloud.mystarer.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author syl
 * @create 2018-07-26 16:47
 **/
@ConfigurationProperties("example.component")
public class StarterServiceProperties {
    private String config;

    public void setConfig(String config) {
        this.config = config;
    }

    public String getConfig() {
        return config;
    }

}
