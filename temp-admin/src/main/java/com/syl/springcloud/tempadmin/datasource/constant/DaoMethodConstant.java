package com.syl.springcloud.tempadmin.datasource.constant;

/**
 * @author syl
 * @create 2018-09-02 15:10
 **/
public class DaoMethodConstant {

    public static final String INSERT_TABLE__METHOD_NAME = "createTable";
    public static final String TABLE_METHOD_NAME = "selectTableNameList";
    public static final String COLUMN_METHOD_NAME = "selectTableColumnList";
}
