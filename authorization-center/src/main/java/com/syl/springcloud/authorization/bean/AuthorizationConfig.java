package com.syl.springcloud.authorization.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author syl
 * @create 2018-08-02 17:24
 **/
@Component
@ConfigurationProperties(prefix = "authorization")
// TODO 这里的${user.dir} 必须和BaseConstant.GLOBAL_CONFIGS_PATH 的值一样
@PropertySource("file:/${user.dir}/global-configs/authorization-config.properties")
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class AuthorizationConfig {
    private String clientId;
    private String secret;
    private List<String> grantTypes = new ArrayList<>();
    private List<String> scopes = new ArrayList<>();
    private boolean autoApprove;
    private List<String> registeredSSoLoginUri = new ArrayList<>();
    private int tokenValidityS;
    private Set<String> unimportance = new HashSet<>();

}
