package com.syl.springcloud.moredb.user.controller;

import com.syl.springcloud.moredb.user.bean.SystenUser;
import com.syl.springcloud.moredb.user.service.SystenUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author syl
 * @create 2018-08-29 16:21
 **/
@RestController
@RequestMapping("systen/user")
public class SystenUserController {
    @Autowired
    private SystenUserService service;

    @RequestMapping("/get")
    public String get(){
        List<SystenUser> users = service.selectAll();
        System.out.println(users);
        return "ok";
    }

    @RequestMapping("/test")
    public String test(){
        service.testTransaction();
        return "ok";
    }

}
