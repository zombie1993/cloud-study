package com.syl.springcloud.authorization.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * @author syl
 * @create 2018-08-10 11:45
 **/
@RestController
@RequestMapping("home")
public class HomeController {

    @GetMapping("/index")
    public String index(Principal principal){
        System.out.println(principal);
        return "登录成功";
    }

    @GetMapping("/get/id")
    public String getSessionId(HttpServletRequest request){
        return request.getSession().getId();
    }

}
