package com.syl.springcloud.mystarer.config;

import com.syl.springcloud.mystarer.controller.BaseController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author syl
 * @create 2018-07-27 9:51
 **/
@Configuration
public class BaseControllerConfig {

    @Bean
    public BaseController baseController(){
        return new BaseController();
    }

}
