### 服务名称 
授权中心

### 服务描述
0. 使用consul作为所有服务的调度中心
1. 从数据库读取用户匹配登录
2. 作为所有服务的用户的授权中心(oauth2)

### 使用须知
0. 需保证配置中心已启动

### 授权说明
#### 方式一: 授权码模式 
0. [获取授权码url](http://127.0.0.1:17002/oauth2/oauth/authorize?client_id=iadmin&response_type=code&redirect_uri=http://www.baidu.com)
注意: redirect_uri 做了限制 必须在配置的列表中才允许获取
1. 获取token
curl post 127.0.0.1:17002/oauth2/oauth/token "application/x-www-form-urlencoded" -d 
{\
   "code" : "授权码",\
   "grant_type" : "authorization_code",\
   "client_id" : "client_id",\
   "client_secret" : "client_secret",\
   "redirect_uri" : "redirect_uri"\
}
#### 方式二: 密码模式
0. curl post 127.0.0.1:17002/oauth2/oauth/token "application/x-www-form-urlencoded" -d {\
  "grant_type" : "password",
  "username" : "user",
  "password" : "password",
  "client_id" : "client_id",
  "client_secret" : "client_secret"
}

### 登录说明
0. 推荐使用json 发送post请求做登录 以实现前后端分离
1. 使用json登录时参数需要做RAS 加密(请求/aes/get/secret/key) 来获取公钥
2. 已实现密码模式,密码图形验证码模式,短信模式,如需扩展参照LoginTypeEnum类,然后在CustomAuthenticationFilter实现扩展方法

### 前端流程 TODO 后台需增加一个获取access_token的方法方便调用
0. 从登录页面发送post 请求登录
1. 请求获取oauth2 code [url](http://127.0.0.1:17002/oauth2/oauth/authorize?client_id=iadmin&response_type=code&redirect_uri=)
注意： redirect_uri 需要在 配置 registeredSSoLoginUri  不推荐使用这种方式手动获取access_token 推荐使用2 
2. 请求http://127.0.0.1:17002/oauth2/token/get/access_token/{iadmin} 获取access_token (token 2小时过期)
3. 获取资源时必须带上access_token 方可访问 
4. 如果资源需要不做oauth2授权 登录即可访问在authorization-config.properties 修改unimportance值
