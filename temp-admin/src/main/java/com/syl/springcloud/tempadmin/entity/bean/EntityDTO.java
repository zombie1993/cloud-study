package com.syl.springcloud.tempadmin.entity.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体 DTO
 *
 * @author syl
 * @create 2018-09-03
 *
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class EntityDTO implements Serializable {

    public EntityDTO(){}

    /**
     * id
     */
    private String id;
    /**
     * 实体名称
     */
    private String name;
    /**
     * 业务id
     */
    private String businessId;
    /**
     * 业务编码
     */
    private String businessCode;
    /**
     * 数据表名称
     */
    private String tableName;
    /**
     * 数据源
     */
    private String datasource;
    /**
     * 实体类型
     */
    private Integer type;
    /**
     * 来源
     */
    private Integer source;
    /**
     * 是否删除
     */
    private Boolean delete;
    /**
     * 是否同步
     */
    private Boolean sync;
    /**
     * 是否配置
     */
    private Boolean config;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 备注
     */
    private String remark;
    /**
     * 按create_time的开始时间查找
     */
    private Date createTimeStart;
    /**
     * 按create_time的截止时间查找
     */
    private Date createTimeEnd;
    /**
     * 按update_time的开始时间查找
     */
    private Date updateTimeStart;
    /**
     * 按update_time的截止时间查找
     */
    private Date updateTimeEnd;

}
