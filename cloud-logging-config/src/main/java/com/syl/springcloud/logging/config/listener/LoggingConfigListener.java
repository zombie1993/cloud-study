package com.syl.springcloud.logging.config.listener;

import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * 日志系统配置
 *
 * @author syl
 * @create 2018-09-06 14:15
 **/
public class LoggingConfigListener implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {

    @Override
    public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
        ConfigurableEnvironment environment = event.getEnvironment();
        MutablePropertySources propertySources = environment.getPropertySources();
        Map<String, Object> myMap = new HashMap();
        URL url = LoggingConfigListener.class.getClassLoader().getResource("");
        String path = url.getPath()+ File.separator + "logback-spring.xml";
        System.out.println(path);
        myMap.put("logging.config", path);
        propertySources.addFirst(new MapPropertySource("loggingConfig", myMap));
    }

}
