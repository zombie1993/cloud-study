package com.syl.springcloud.parent.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.syl.springcloud.parent.controller.BaseController;

/**
 * @author syl
 * @create 2018-07-27 9:51
 **/
@Configuration
public class BaseControllerConfig {

    @Bean
    public BaseController baseController(){
        return new BaseController();
    }

}
