### 项目名称 
 基础 spring security starter

### 项目描述
0. 为需要使用登录的项目配置spring security oauth2最基本的几个配置项
1. 统一管理oauth2 授权服务器所必须的密钥等关键信息

### 使用须知
0. 所有需要登录的分布式服务的基础starter