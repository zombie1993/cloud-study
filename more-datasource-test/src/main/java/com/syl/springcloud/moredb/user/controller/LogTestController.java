package com.syl.springcloud.moredb.user.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author syl
 * @create 2018-09-06 11:11
 **/
@RestController
@RequestMapping("/log")
public class LogTestController {
    private static Logger LOG = LoggerFactory.getLogger(LogTestController.class);
    @GetMapping
    public String test(){
        Map<String,String> map = new HashMap<String,String>();
        map.put("a", "b");
        LOG.info("[]","abc");
        LOG.info("{}", map);
        LOG.info("[%s]","222222222222");
        LOG.error("====================",new Exception("模拟错误"));
        LOG.debug("====================");
        LOG.warn("====================");
        return "ok";
    }

}
