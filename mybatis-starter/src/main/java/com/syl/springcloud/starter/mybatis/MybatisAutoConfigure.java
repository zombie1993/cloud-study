package com.syl.springcloud.starter.mybatis;

import com.syl.springcloud.starter.mybatis.bean.DataSourceProperties;
import com.syl.springcloud.starter.mybatis.component.DataSourceAspect;
import com.syl.springcloud.starter.mybatis.config.MyBatisConfig;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

/**
 * @author syl
 * @create 2018-08-29 19:59
 **/
@Import({
    MyBatisConfig.class,
    DataSourceAspect.class
})
@EnableConfigurationProperties(DataSourceProperties.class)
public class MybatisAutoConfigure {

}
