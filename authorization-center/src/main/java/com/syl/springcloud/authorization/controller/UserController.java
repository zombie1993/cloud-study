package com.syl.springcloud.authorization.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * @author syl
 * @create 2018-08-08 15:36
 **/
@RequestMapping("user")
@RestController
public class UserController {

    @Autowired
    private RedisOperationsSessionRepository sessionRepository;

    @GetMapping("/info")
    public UserDetails userInfo(){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDetails;
    }

    @GetMapping("/me")
    public Principal user(Principal principal) {
        return principal;
    }
}
