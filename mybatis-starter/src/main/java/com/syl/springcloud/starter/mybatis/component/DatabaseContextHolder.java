package com.syl.springcloud.starter.mybatis.component;

import com.syl.springcloud.starter.mybatis.enums.DataSourceEnum;
import org.springframework.stereotype.Component;

/**
 *  数据源枚举安全线程池
 * @author syl
 * @create 2018-08-29 16:37
 **/
@Component
public class DatabaseContextHolder {
    private static final ThreadLocal<DataSourceEnum> contextHolder = new ThreadLocal<>();

     public static void setDatabaseType(DataSourceEnum type){
         contextHolder.set(type);
     }

     public static DataSourceEnum getDatabaseType(){
         return contextHolder.get();
     }

    public static void clear() {
        contextHolder.remove();
    }
}
