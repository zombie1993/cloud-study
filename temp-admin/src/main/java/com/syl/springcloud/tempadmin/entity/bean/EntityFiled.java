package com.syl.springcloud.tempadmin.entity.bean;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;
import lombok.ToString;
import java.lang.Integer;
import java.lang.Boolean;
import java.io.Serializable;
import lombok.experimental.Accessors;

/**
 * 实体字段属性 POJO
 *
 * @author syl
 * @create 2018-09-02
 *  
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class EntityFiled implements Serializable {
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String ENTITY_ID = "entity_id";
    public static final String BUSINESS_ID = "business_id";
    public static final String BUSINESS_CODE = "business_code";
    public static final String FILED = "filed";
    public static final String LENGTH = "length";
    public static final String TYPE = "type";
    public static final String DICTIONARY_ID = "dictionary_id";
    public static final String DATA_FORMAT = "data_format";
    public static final String DEFAULT_VALUE = "default_value";
    public static final String IS_PRIMARY_KEY = "is_primary_key";
    public static final String IS_DELETE = "is_delete";
    public static final String SEQUENCE = "sequence";
    public static final String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String REMARK = "remark";

    public EntityFiled(){}

    /**
     * id
     */
    private String id;
    /**
     * 字段名称
     */
    private String name;
    /**
     * 实体id
     */
    private String entityId;
    /**
     * 业务id
     */
    private String businessId;
    /**
     * 业务编码
     */
    private String businessCode;
    /**
     * 字段
     */
    private String filed;
    /**
     * 字段长度
     */
    private Integer length;
    /**
     * 字段类型
     */
    private Integer type;
    /**
     * 字典id
     */
    private String dictionaryId;
    /**
     * 数据格式化
     */
    private String dataFormat;
    /**
     * 默认值 需要支持几个内置方法
     */
    private String defaultValue;
    /**
     * 是否主键
     */
    private Boolean primaryKey;
    /**
     * 是否删除
     */
    private Boolean delete;
    /**
     * 顺序
     */
    private Integer sequence;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 备注
     */
    private String remark;

}
