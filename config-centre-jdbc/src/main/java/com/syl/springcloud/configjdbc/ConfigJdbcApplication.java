package com.syl.springcloud.configjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author syl
 * @create 2018-07-19 15:47
 **/
@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigServer
public class ConfigJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigJdbcApplication.class, args);
    }

}
