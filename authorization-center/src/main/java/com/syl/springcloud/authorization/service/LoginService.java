package com.syl.springcloud.authorization.service;

import com.syl.springcloud.authorization.bean.DoubleResult;
import org.patchca.service.ConfigurableCaptchaService;

/**
 * @author syl
 * @create 2018-08-07 10:08
 **/
public interface LoginService {

    /**
     *初始化验证码配置
     * @param width
     * @param height
     * @param single 是否单调颜色
     * @return
     */
    ConfigurableCaptchaService initCaptchaConfig(int width,int height,boolean single);

    /**
     * 验证验证码
     * @param inputCode 用户输入的码
     * @param captcha 待验证的码
     * @return 状态说明 0: 成功 10: 内容为空 20: 验证失败
     */
    DoubleResult<Integer,String> verifyCaptcha(String inputCode, String captcha);
}
