//package com.syl.springcloud.tempadmin.entity.service.impl;
//
//import com.syl.framework.common.BaseDao;
//import com.syl.framework.common.BaseServiceImpl;
//import com.syl.springcloud.starter.mybatis.config.MyBatisConfig;
//import com.syl.springcloud.starter.mybatis.enums.DataSourceEnum;
//import com.syl.springcloud.tempadmin.datasource.bean.Schema;
//import com.syl.springcloud.tempadmin.datasource.service.SchemaTableService;
//import com.syl.springcloud.tempadmin.entity.bean.Entity;
//import com.syl.springcloud.tempadmin.entity.bean.EntityDTO;
//import com.syl.springcloud.tempadmin.entity.enums.EntityTypeEnum;
//import com.syl.springcloud.tempadmin.entity.service.EntityService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Service;
//import org.springframework.util.StringUtils;
//
//import javax.sql.DataSource;
//import java.util.*;
//
///**
// * 基础实体表 Service 实现类
// *
// * @author syl
// * @create 2018-09-02
// *
// **/
//@Service
//public class EntityServiceImpl extends BaseServiceImpl<Entity,EntityDTO> implements EntityService{
//    private static final Logger LOG = LoggerFactory.getLogger(EntityServiceImpl.class);
//
//    @Autowired
//    private MyBatisConfig myBatisConfig;
//    @Autowired
//    private SchemaTableService tableService;
//
//    //Business code
//    @Override
//    public int syncSchema() {
//        Map<Object, Object> dataSourceMap = myBatisConfig.getAllDataSource(true);
//        Set<Object> keySet = dataSourceMap.keySet();
//        List<Entity> result = new ArrayList<>(100);
//        for (Object o : keySet) {
//            String key = o.toString();
//            DataSourceEnum dbEnum = DataSourceEnum.getEnumByName(key);
//            DataSource dataSource = (DataSource) dataSourceMap.get(key);
//            List<Schema> schemaList = tableService.selectAllTable(dbEnum, dataSource);
//            if(schemaList == null)continue;
//            List<Entity> tempList = modelTransform(schemaList, dbEnum);
//            result.addAll(filterRepetition(tempList));
//        }
//        List<String> list = this.saveList(result);
//        return list.size();
//    }
//
//    @Override
//    public String save(Entity entity) {
//        entity.setId(UUID.randomUUID().toString()).setSource(1)
//        .setDelete(false).setCreateTime(new Date()).setSync(false)
//        .setConfig(false)
//        ;
//        long l = dao.insertSelective(entity);
//        LOG.info("保存实体结果："+ l);
//        return entity.getId();
//    }
//
//    /**
//     * 过滤重复的表 依据数据库名称 和 表名
//     * @param entityList
//     * @return
//     */
//    private List<Entity> filterRepetition(List<Entity> entityList){
//        List<Entity> allList = this.selectAll();
//        List<Entity> newList = new ArrayList<>();
//        for (Entity entity : entityList) {
//            if(allList.contains(entity))continue;
//            newList.add(entity);
//        }
//        LOG.info("过滤了 "+ (entityList.size() - newList.size()));
//        return newList;
//    }
//
//    /**
//     * 把DataSourceBean 模型转化为Entity
//     * @param schemaList
//     * @param dbEnum
//     * @return
//     */
//    private List<Entity> modelTransform(List<Schema> schemaList, DataSourceEnum dbEnum){
//        List<Entity> entityList = new ArrayList<>(schemaList.size());
//        String businessCode = "iadmin";//TODO 业务编码 登录后保存到session 暂时模拟
//        String businessId = UUID.randomUUID().toString();//TODO 业务id 通过业务编码获取 暂时模拟
//        for (Schema sourceBean : schemaList) {
//            String comment = sourceBean.getTableComment();
//            String tableName = sourceBean.getTableName();
//            Entity entity = new Entity()
//            .setName(StringUtils.isEmpty(comment) ? tableName : comment).setBusinessId(businessId)
//            .setBusinessCode(businessCode).setDatasource(dbEnum.getName())
//            .setType(EntityTypeEnum.DB.getCode()).setTableName(tableName)
//            .setSource(0).setSync(true).setConfig(false)
//            ;
//            entityList.add(entity);
//        }
//        return entityList;
//    }
//
//    @Override
//    @Autowired
//    @Qualifier("entityDao")
//    public void setDao(BaseDao dao) {
//        super.setDao(dao);
//    }
//
//}