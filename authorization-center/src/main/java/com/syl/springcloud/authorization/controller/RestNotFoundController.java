package com.syl.springcloud.authorization.controller;

import com.syl.springcloud.authorization.bean.ReturnMessage;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author syl
 * @create 2018-08-15 10:31
 **/
@RestController
public class RestNotFoundController implements ErrorController {

    @RequestMapping("/error")
    public ReturnMessage handleError(HttpServletRequest request) {
        //获取statusCode:401,404,500
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        switch (statusCode){
            case 400:
                return new ReturnMessage(statusCode,"请求参数有误");
            case 401:
                return new ReturnMessage(statusCode,"您没有登录或登录已超时");
            case 403:
                return new ReturnMessage(statusCode,"权限不足,已拒绝访问");
            case 404:
                return new ReturnMessage(statusCode, "找不到页面");
            case 500:
            default:
                return new ReturnMessage(statusCode, "服务器开小差了");
        }
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

}
