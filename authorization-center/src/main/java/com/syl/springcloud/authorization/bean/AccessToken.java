package com.syl.springcloud.authorization.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author syl
 * @create 2018-08-10 10:35
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class AccessToken implements Serializable {
    private String access_token;
    private String token_type;
    private String refresh_token;
    private int expires_in;
//    private String[] scope;
}
