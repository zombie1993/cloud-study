package com.syl.springcloud.tempadmin.datasource.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 数据库表列信息
 * @author syl
 * @create 2018-04-01 21:12
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class Schema implements Serializable {

    /**
     * 数据库
     */
    private String schema;
    /**
     * 表名
     */
    private String tableName;
    /**
     * 表注释
     */
    private String tableComment;

    public Schema() {
    }
}
