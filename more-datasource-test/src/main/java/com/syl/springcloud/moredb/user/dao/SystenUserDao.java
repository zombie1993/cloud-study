package com.syl.springcloud.moredb.user.dao;

import com.syl.framework.common.BaseDao;
import com.syl.springcloud.moredb.user.bean.SystenUser;
import com.syl.springcloud.moredb.user.bean.SystenUserDTO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* 系统用户表类 Dao
*
* @author syl
* @create 2018-08-28
*
**/
@Mapper
@Repository
public interface SystenUserDao extends BaseDao<SystenUser,SystenUserDTO> {
    //Business code

}
