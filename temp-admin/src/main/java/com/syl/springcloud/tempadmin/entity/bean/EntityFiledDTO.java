package com.syl.springcloud.tempadmin.entity.bean;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;
import lombok.ToString;
import java.lang.Integer;
import java.lang.Boolean;
import java.io.Serializable;
import lombok.experimental.Accessors;

/**
 * 实体字段属性 DTO
 *
 * @author syl
 * @create 2018-09-02
 *  
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class EntityFiledDTO implements Serializable {

    public EntityFiledDTO(){}

    /**
     * id
     */
    private String id;
    /**
     * 字段名称
     */
    private String name;
    /**
     * 实体id
     */
    private String entityId;
    /**
     * 业务id
     */
    private String businessId;
    /**
     * 业务编码
     */
    private String businessCode;
    /**
     * 字段
     */
    private String filed;
    /**
     * 字段长度
     */
    private Integer length;
    /**
     * 字段类型
     */
    private Integer type;
    /**
     * 字典id
     */
    private String dictionaryId;
    /**
     * 数据格式化
     */
    private String dataFormat;
    /**
     * 默认值 需要支持几个内置方法
     */
    private String defaultValue;
    /**
     * 是否主键
     */
    private Boolean primaryKey;
    /**
     * 是否删除
     */
    private Boolean delete;
    /**
     * 顺序
     */
    private Integer sequence;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 备注
     */
    private String remark;
    /**
     * 按create_time的开始时间查找
     */
    private Date createTimeStart;
    /**
     * 按create_time的截止时间查找
     */
    private Date createTimeEnd;
    /**
     * 按update_time的开始时间查找
     */
    private Date updateTimeStart;
    /**
     * 按update_time的截止时间查找
     */
    private Date updateTimeEnd;

}
