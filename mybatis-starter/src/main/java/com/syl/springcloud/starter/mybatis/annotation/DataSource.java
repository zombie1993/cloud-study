package com.syl.springcloud.starter.mybatis.annotation;

import com.syl.springcloud.starter.mybatis.enums.DataSourceEnum;

import java.lang.annotation.*;

/**
 * 数据源注解 只可使用于service层
 * @author syl
 * @create 2018-08-29 17:28
 **/
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {

    DataSourceEnum value() default DataSourceEnum.IADMIN;
}
