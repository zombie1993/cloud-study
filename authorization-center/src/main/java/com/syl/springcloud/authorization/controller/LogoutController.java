package com.syl.springcloud.authorization.controller;

import com.syl.springcloud.authorization.bean.ReturnMessage;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author syl
 * @create 2018-08-14 13:47
 **/
@RestController
@RequestMapping("/logout")
public class LogoutController {

    @RequestMapping("/")
    public ReturnMessage logout(){
        return new ReturnMessage(200,"退出登录成功");
    }


}
