package com.syl.springcloud.configjdbc;

import com.syl.springcloud.configjdbc.bean.UserInfo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author syl
 * @create 2018-07-19 15:47
 **/
@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigServer
@EnableConfigurationProperties({UserInfo.class})
public class ConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigApplication.class, args);
    }

}
