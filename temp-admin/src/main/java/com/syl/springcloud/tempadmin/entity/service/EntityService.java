package com.syl.springcloud.tempadmin.entity.service;

import com.syl.framework.common.BaseService;
import com.syl.springcloud.tempadmin.entity.bean.Entity;
import com.syl.springcloud.tempadmin.entity.bean.EntityDTO;

/**
 * 基础实体表 Service
 *
 * @author syl
 * @create 2018-09-02
 *  
 **/
public interface EntityService extends BaseService<Entity,EntityDTO> {

    //Business code

    /**
     * 同步数据源
     * @return 共同步了多少条数据
     */
    int syncSchema();

    /**
     * 新增实体
     * @param entity
     * @return
     */
    String save(Entity entity);
}