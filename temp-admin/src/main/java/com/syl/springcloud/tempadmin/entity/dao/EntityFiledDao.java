package com.syl.springcloud.tempadmin.entity.dao;

import com.syl.framework.common.BaseDao;
import com.syl.springcloud.tempadmin.entity.bean.EntityFiled;
import com.syl.springcloud.tempadmin.entity.bean.EntityFiledDTO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* 实体字段属性类 Dao
*
* @author syl
* @create 2018-09-02
*  
**/
@Mapper
@Repository
public interface EntityFiledDao extends BaseDao<EntityFiled,EntityFiledDTO> {
    //Business code


}
