package com.syl.springcloud.starter.mybatis.enums;

/**
 * TODO 更改项目时需要更改该枚举
 * @author syl
 * @create 2018-08-29 16:34
 *  h|name|dbType
 *  -|-|-
 *  IADMIN|iadmin|mysql
 *  BIZ1|biz1|mysql
 **/
public enum DataSourceEnum {
    IADMIN("iadmin", "MySql"),
    BIZ1("biz1", "MySql"),
    NULL(null, null);

    private String name;
    /**
     * 必须和xxxDataSourceDao 接口的xxx命名一致
     */
    private String dbType;

    DataSourceEnum(String name, String dbType) {
        this.name = name;
        this.dbType = dbType;
    }

    public static DataSourceEnum getEnumByName(String name) {
        DataSourceEnum[] values = DataSourceEnum.values();
        for (DataSourceEnum em : values) {
            String emName = em.getName();
            if (emName == null) continue;
            if (emName.equals(name)) return em;
        }
        return NULL;
    }

    public static DataSourceEnum getEnumByDbType(String dbType) {
        DataSourceEnum[] values = DataSourceEnum.values();
        for (DataSourceEnum em : values) {
            String emDbType = em.getDbType();
            if (emDbType == null) continue;
            if (emDbType.equals(dbType)) return em;
        }
        return NULL;
    }

    public String getName() {
        return name;
    }

    public String getDbType() {
        return dbType;
    }
}
