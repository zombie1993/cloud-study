package com.syl.springcloud.authorization.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.security.RolesAllowed;
import java.security.Principal;

/**
 * @author syl
 * @create 2018-07-27 11:08
 **/
@RestController
@RequestMapping("welcome")
public class TestController {

    @GetMapping("/test")
    public String test(){
        return"hello";
    }

    @GetMapping("/library")
    public String library() {
        return "welcome to library";
    }



}
