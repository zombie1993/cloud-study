package com.syl.springcloud.authorization.constant;

/**
 * @author syl
 * @create 2018-08-06 15:46
 **/
public class LoginConstant {

    public static final String BCRYPT = "{bcrypt}";

    public static final String DB_PASSWORD = "{db_password}";

    public static final String IMAGE_TYPE_PNG = "png";

    /**
     * 测试时使用的模拟密码 1234
     */
    public static final String TEST_MIMA = "$2a$10$h6mT7xVgFD8bV2fD8s5ZUOZ7G4HzHEU4JPUIWzeCbqi5R5NsK8Zoa";

    /**
     * 登录密码加密公钥 生成传送门 http://web.chacuo.net/netrsakeypair
     */
    public static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCoZK6jq+UOHKVoXZ5DAIV8h5HD\n" +
                "QZTUZe5dBX5phv+B8XiGsRYGCf7Ywsg7YxuB/Rlo3N11PGOEuak1zVlAAms6XxXg\n" +
                "p4VQrgvIIVlFAqIPszb2x4HhRpO23ateOlDun/CXyKQuqjtZ+ow4VkDB22r86cDp\n" +
                "lvL0B3Dso0+M8xz/SwIDAQAB";

    /**
     * 登录密码解密私钥
     */
    public static final String PRIVATE_KEY = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKhkrqOr5Q4cpWhd\n" +
            "nkMAhXyHkcNBlNRl7l0FfmmG/4HxeIaxFgYJ/tjCyDtjG4H9GWjc3XU8Y4S5qTXN\n" +
            "WUACazpfFeCnhVCuC8ghWUUCog+zNvbHgeFGk7bdq146UO6f8JfIpC6qO1n6jDhW\n" +
            "QMHbavzpwOmW8vQHcOyjT4zzHP9LAgMBAAECgYEAoAmYBEtg1OmatjaLK6h6NODv\n" +
            "Qj+4v6fkQxnui0JW4NtqLbBteu3S+5UbEX3ebcrqdEO8YSg0eLauv0hbMz9ZQcTl\n" +
            "DK8Pdv+yFKKI3s8c4Svn7/7c0farEYLH7IweXt/cDmuNdqbe5/b7dQR5tKxgtcEz\n" +
            "UAuijUUCNR7Xf6kU+7ECQQDRBVi4TlnxTgZ/L4MsGmD+IOy9A5xGzmJNukhX0do3\n" +
            "h7kYZdrKx7Q3p53WBNVFkE+p/ElhNO2xRI/d9oJQzam5AkEAzj20Acq7NZ/L1RCq\n" +
            "3+5hnlT4T4gxLp9GGF81A8fYfDVeYkQyMWjkVrAPO7YC2HUaM3ZIDUFFJTxrkCXS\n" +
            "sl6jIwJASRwQoy6soQ2EkjoBLIzxfhHwHFDF+bI4ii1tHmKFbETpxhHiTGk7rPfQ\n" +
            "6kn4ewN2CeP3mquTtBxiIeYWQMMe6QJAesi/ZyW0KOu2pWWj5Wd9VI8PugHCDhii\n" +
            "2ZWs8VBEMUZwp54kJ8hj0LxGhW0J6qZk7+hmYwXNzapX+1yZQLoY8wJBAIk2D59d\n" +
            "JVraHCTsFv+w1mthCRLmRM3KfyTqd6CXt8UZ+HeJL8hG4k/PBzVTDrafmIJJvmiV\n" +
            "+fCIoyAXk9zWlAE=";
}
