package com.syl.springcloud.tempadmin.entity.enums;

/**
 * 实体类型枚举
 *
 * @author syl
 * @create 2018-09-02 16:00
 *
 *  h|code|name
 *  -|-|-
 *  DB| 1 | 数据库bean对象
 *  JAVA_BEAN | 2 | 普通bean对象
 **/
public enum EntityTypeEnum {
    DB(1, "数据库bean对象"),
    JAVA_BEAN(2, "普通bean对象"),
    NULL(null, null);

    private Integer code;
    private String name;

    EntityTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public static EntityTypeEnum getEnumByCode(Integer code) {
        EntityTypeEnum[] values = EntityTypeEnum.values();
        for (EntityTypeEnum em : values) {
            Integer emCode = em.getCode();
            if (emCode == null) continue;
            if (emCode.equals(code)) return em;
        }
        return NULL;
    }

    public static EntityTypeEnum getEnumByName(String name) {
        EntityTypeEnum[] values = EntityTypeEnum.values();
        for (EntityTypeEnum em : values) {
            String emName = em.getName();
            if (emName == null) continue;
            if (emName.equals(name)) return em;
        }
        return NULL;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
