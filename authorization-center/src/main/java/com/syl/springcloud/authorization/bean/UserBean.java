package com.syl.springcloud.authorization.bean;

import com.google.gson.annotations.SerializedName;
import com.syl.springcloud.authorization.enums.LoginTypeEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author syl
 * @create 2018-08-06 15:08
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class UserBean {
    private String username;
    private String phone;
    private String password;
    @SerializedName("code")
    private String verifyCode;
    private LoginTypeEnum type;

}
