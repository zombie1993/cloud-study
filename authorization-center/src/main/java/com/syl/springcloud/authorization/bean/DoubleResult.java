package com.syl.springcloud.authorization.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author syl
 * @create 2018-08-07 17:10
 **/
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class DoubleResult<state, data> {
    public state state;
    public data data;

    public DoubleResult(state state, data data) {
        this.state = state;
        this.data = data;
    }
}
