package com.syl.springcloud.tempadmin.entity.controller;

import com.syl.springcloud.starter.mybatis.enums.DataSourceEnum;
import com.syl.springcloud.tempadmin.entity.bean.Entity;
import com.syl.springcloud.tempadmin.entity.enums.EntityTypeEnum;
import com.syl.springcloud.tempadmin.entity.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

/**
 * @author syl
 * @create 2018-09-02 12:06
 **/
@RestController
@RequestMapping("/entity")
public class EntityController {

    @Autowired
    private EntityService entityService;

    @RequestMapping("/get")
    public List<Entity> getDataSource(){
        List<Entity> entityList = entityService.selectAll();
        return entityList;
    }

    @RequestMapping("/sync")
    public String syncDataSource(){
        int i = entityService.syncSchema();
        System.out.println("-----------**********************-----------");
        return i+"";
    }

    @RequestMapping("/create")
    public String save(){
        String businessCode = "iadmin";//TODO 业务编码 登录后保存到session 暂时模拟
        String businessId = UUID.randomUUID().toString();//TODO 业务id 通过业务编码获取 暂时模拟
        String id = entityService.save(
            new Entity().setTableName("test_table").setName("测试")
            .setBusinessCode(businessCode).setBusinessId(businessId)
            .setDatasource(DataSourceEnum.IADMIN.getName()).setType(EntityTypeEnum.DB.getCode())
        );
        return id;
    }

}
