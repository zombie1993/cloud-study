//package com.syl.springcloud.tempadmin.entity.service.impl;
//
//import com.syl.framework.common.BaseDao;
//import com.syl.framework.common.BaseServiceImpl;
//import com.syl.framework.common.Criteria;
//import com.syl.springcloud.starter.mybatis.config.MyBatisConfig;
//import com.syl.springcloud.starter.mybatis.enums.DataSourceEnum;
//import com.syl.springcloud.tempadmin.datasource.bean.SchemaColumns;
//import com.syl.springcloud.tempadmin.datasource.service.SchemaTableService;
//import com.syl.springcloud.tempadmin.entity.bean.Entity;
//import com.syl.springcloud.tempadmin.entity.bean.EntityFiled;
//import com.syl.springcloud.tempadmin.entity.bean.EntityFiledDTO;
//import com.syl.springcloud.tempadmin.entity.service.EntityFiledService;
//import com.syl.springcloud.tempadmin.entity.service.EntityService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Service;
//
//import javax.sql.DataSource;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//import java.util.UUID;
//
///**
// * 实体字段属性 Service 实现类
// *
// * @author syl
// * @create 2018-09-02
// *
// **/
//@Service
//public class EntityFiledServiceImpl extends BaseServiceImpl<EntityFiled,EntityFiledDTO> implements EntityFiledService {
//
//    @Autowired
//    private MyBatisConfig myBatisConfig;
//    @Autowired
//    private EntityService entityService;
//    @Autowired
//    private SchemaTableService schemaTableService;
//
//    @Override
//    public List<EntityFiled> queryEntityFiled(String entityId) {
//        Entity entity = entityService.selectById(
//            new Criteria().addQuery(Entity.TABLE_NAME).addQuery(Entity.DATASOURCE),
//            entityId
//        );
//        if(entity == null)return Collections.EMPTY_LIST;
//        DataSourceEnum dataSourceEnum = DataSourceEnum.getEnumByName(entity.getDatasource());
//        DataSource dataSource = myBatisConfig.getDataSource(dataSourceEnum);
//        List<SchemaColumns> columnsList = schemaTableService.selectAllColumn(entity.getTableName(), dataSourceEnum, dataSource);
//        return modelTransform(columnsList,dataSourceEnum);
//    }
//
//    /**
//     * 把SchemaColumns 模型转化为EntityFiled
//     * @param columnsList
//     * @param dbEnum
//     * @return
//     */
//    private List<EntityFiled> modelTransform(List<SchemaColumns> columnsList,DataSourceEnum dbEnum){
//        List<EntityFiled> list = new ArrayList<>();
//        String businessCode = "iadmin";//TODO 业务编码 登录后保存到session 暂时模拟
//        String businessId = UUID.randomUUID().toString();//TODO 业务id 通过业务编码获取 暂时模拟
//        int i = 0;
//        for (SchemaColumns columns : columnsList) {
//            String type = columns.getColumnType();
//            type = type.substring(0,type.length()-1);
//            String[] types = type.split("\\(");
//            EntityFiled entityFiled = new EntityFiled()
//            .setName(columns.getColumnComment())
//            .setBusinessCode(businessCode).setBusinessId(businessId)
//            .setFiled(columns.getColumnName()).setLength(types.length > 1 ? Integer.valueOf(types[1]) : 0)
//            .setPrimaryKey(columns.isPrimaryKey()).setSequence(i)
//            ;
//            list.add(entityFiled);
//            i++;
//        }
//        return list;
//    }
//
//    @Override
//    @Autowired
//    @Qualifier("entityFiledDao")
//    public void setDao(BaseDao dao) {
//        super.setDao(dao);
//    }
//
//}