package com.syl.springcloud.moredb.user.service;

import com.syl.springcloud.moredb.user.bean.SystenUser;
import com.syl.springcloud.moredb.user.dao.SystenUserDao;
import com.syl.starter.mybatis.moredb.annotation.DataSource;
import com.syl.starter.mybatis.moredb.enums.DataSourceEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author syl
 * @create 2018-08-29 19:16
 **/
@Service
//@DataSource
public class SystenUserService {

    @Autowired
    private SystenUserDao dao;

    public List<SystenUser> selectAll(){
        return dao.selectAll();
    }

    @Transactional
    public void testTransaction(){
        long aaa = dao.insertSelective(
            new SystenUser().setId(UUID.randomUUID().toString()).setName("AAA").setCreateTime(new Date())
        );
//        int i = dao.updateByColumn(new Criteria("aa"), new SystenUserDTO().setName("aaa"), "2");
        System.out.println("aaa: "+aaa +"i: "+mockError());
    }

    private int mockError(){
        SystenUser user = new SystenUser();
        user.getName().toString();
        return 1;
    }
}
