package com.syl.springcloud.tempadmin.datasource.dao;

import javax.annotation.Resource;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据源dao
 *
 * @author syl
 * @create 2018-09-02 14:56
 **/
@Resource
@Mapper
public interface MySqlDataSourceDao extends BaseDataSourceDao{


}
