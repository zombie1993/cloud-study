### 服务名称 
配置服务器

### 服务描述
0. 使用consul作为所有服务的调度中心
1. 使用jdbc作为配置仓库
2. 使用JCE对特定敏感数据进行加密
3. [加密url](127.0.0.1:17001/config/encrypt)
4. 作为所有服务的配置中心

### 使用须知
0. 需安装[consul](https://www.consul.io/downloads.html) 服务 使用 ./consul agent -dev -ui 启动服务
1. 需下载[JCE](http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html)
然后解压到${JDK_PATH}\jre\lib\security
2. 启动配置中心后访问[Test](127.0.0.1:17001/config/encrypt/status) 测试JCE是否正常 
3. 该服务在各分布式服务中启动优先级最高的

### 注意事项
encrypt和decrypt需使用post方式访问 参数以text/plain 方式传递